# Laboratorio_de_Fisica

[Desarrollo de instrumentos abiertos y libres para laboratorio de física](https://osf.io/e7nxd/).

## ¿Por qué?
Creemos que en la enseñanza de ciencias básicas (física, química, biología, geología, etc.) el laboratorio es un espacio de generación de conocimiento
en el se aprende jugando, haciendo con el otro, compartiendo visiones y saberes. Sin embargo, el laboratorio ha ido perdiendo lugar en los espacios
de enseñanza. Esto se debe en gran medida a que equipos comerciales para laboratorio son cerrados y por lo tanto imponen estructuras rígidas a las
prácticas, convirtiéndolas en una especie de receta de cocina en la que lo que se busca es reproducir resultados preestablecidos.
Por otro lado, dichos equipos comereciales tienen un costo elevado y por tanto resultan inaccesibles para muchas instituciones.

## ¿Quiénes?
La propuesta de este proyecto tiene origen en la [Facultad de Ciencias Exactas y Naturales de la Universidad Nacional de Cuyo (FCEN)](https://fcen.uncuyo.edu.ar/)
a partir de la necesidad de crear un laboratorio de física. A la fecha, tenemos desarrollados y [publicados](https://osf.io/e7nxd/) todos los equipos, las interfaces
interactivas y las guías de laboratorio para implementar las prácticas de Física 1, tanto en nivel medio como universitario.
Durante la [primera residencia de tecnologías libres](https://benfeitoria.com/residenciateclivre?ref=postrelogioblog) se conformó un equipo de trabajo
interdisciplinario con miembros de la [red latinoamericana de tecnologías libres (reGOSH)](http://www.cyted.org/es/regosh). Los miembros activos en este proyecto
actualmete son: @pcremades, @andrea.rincon, @daniel.acosta, @martin.ona, @pstorres, @palmira.benavente y @bessonart.valentina.

## Objetivo
La propuesta es desarrollar una plataforma web interactiva que integre las interfaces de todos los equipos. El objetivo es simplificar la interacción
con los instrumentos.
Como objetivo secundario se propone desarrollar algunos accesorios para complementar los equipos actuales y proponer otras prácticas con los 
instrumentos disponibles.

## ¿Para quién?
Este proyecto fue originalmente concebido para la cátedra de física 1 en la [FCEN](https://fcen.uncuyo.edu.ar/), pero las prácticas pueden adaptarse
al nivel medio. Además los equipos sob versátiles y pueden proponerse prácticas apropiadas para otros ámbitos.


El proyecto está en desarrollo. Pueden seguir el progreso en la [wiki](https://tecnologias.libres.cc/andrea.rincon/laboratorio_de_fisica/wikis/Equipos-para-laboratorio-de-f%C3%ADsica:-fotocompuerta).
Pueden colaborar con el proyecto!


## Notas

- Ha quedado diseñado el circuito para que funcione adecuadamente idependientemente de si se usa un fotodiodo o fototransistor. [Enlace](https://tecnologias.libres.cc/andrea.rincon/laboratorio_de_fisica/tree/master/Fotocompuerta)